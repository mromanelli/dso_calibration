# DSO calibration utils
Fork of [mono_dataset_code](https://github.com/tum-vision/mono_dataset_code) that perform camera calibration from a live video stream and produce configuration files for [DSO library](https://github.com/tum-vision/mono_dataset_code).

## Install
##### 1. Install Eigen & OpenCV (if you don't have it):
```
sudo apt-get install libeigen3-dev libopencv-dev
```

##### 2. Build
```
mkdir build && cd build
cmake ..
make
```

## Usage: C++ code
### responseCalib: calibrate response function.
Performs photometric calibration from a live video stream (using OpenCV). Run with:
```
./bin/responseCalib --source 0 --visual true --output pcalib.txt
```
outputs some intermediate results, and pcalib.txt containing the calibrated inverse response function.

The parameters are:

* `-s` or `--source`: the source of the video. A number X specify local devices as /dev/videoX (e.g. 0 for /dev/video0) while a string can represent a rtsp stream (e.g.: rtsp://192.168.42.1). Default is 0.
* `-f` or `--frames`: the number of frames that has to be taken. Default is 50.
* `-v` or `--visual`: if true a window will appear showing the live video stream. Default is false.
* `-o` or `--output`: the gamma calibration file. Default is pcalib.txt.

## License
The code provided in this repository is licensed under the BSD license.