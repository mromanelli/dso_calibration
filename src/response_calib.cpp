/* Copyright (c) 2016, Jakob Engel
 * Modifications Copyright (c) 2018 Marco Romanelli
 * 2018-08-23: Include binary for calibration in DSO library
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation and/or 
 * other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors 
 * may be used to endorse or promote products derived from this software without 
 * specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, 
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 * 
 * @author Marco Romanelli
 */

#include <cstdlib>
#include <iostream>
#include <boost/program_options.hpp>
#include <eigen3/Eigen/Eigen>
#include "opencv2/opencv.hpp"

namespace po = boost::program_options;
using namespace std;

string gamma_calib = "pcalib.txt";			// output gamma calibration file
string video_stream_provider = "0";			// global variable for the output gamma calibration
int n_frames = 50;							// number of frames/images to take
int leakPadding = 2;
int nits = 10;								// number of iterations

cv::VideoCapture cap;						// video device
// vector<cv::Mat> dateset;					// set of frames/images
bool visual = false;						// show frames during acquisition


/** 
 *  @brief  The help print details on how to use the binary and its arguments
 *
 *  @param  desc boost option descriptor
 *  @param  topic boost topic
 *  @return void
 */ 
void ShowHelp(const po::options_description& desc, const string& topic = "")
{
	cout << desc << endl;
	if (topic != "")
	{
		cout << "You asked for help on: " << topic << endl;
	}
	exit(EXIT_FAILURE);
}

/** 
 *  @brief  The help print details on how to use the binary and its arguments
 *
 *  @param  desc boost option descriptor
 *  @param  topic boost topic
 *  @return void
 */ 
void process_program_options(const int argc, const char *const argv[])
{
	po::options_description desc("Usage");
	desc.add_options()
		(
			"help,h",
			po::value<string >()
				->implicit_value("")
				->notifier(
					[&desc](const string& topic) {
						ShowHelp(desc, topic);
					}
				),
			"Show help. If given, show help on the specified topic."
		)
		(
			"source,s",
			po::value<string>(&video_stream_provider),
			"Video source (default: 0 for /dev/video0)"
		)
		(
			"frames,f",
			po::value<int>(&n_frames),
			"Number of frames to take (default: 50)"
		)
		(
			"visual,v",
			po::value<bool>(&visual),
			"Show frames during acquisition (default: false)"
		)
		(
			"output,o",
			po::value<string>(&gamma_calib),
			"Ouput gamma calibration file default: pcalib.txt)"
		);

	po::variables_map args;

	try
	{
		po::store(po::parse_command_line(argc, argv, desc), args);
	}
	catch (po::error const& e)
	{
		cerr << e.what() << endl;
		exit(EXIT_FAILURE);
	}

	po::notify(args);
	return;
}

/** 
 *  @brief  Code copied from main_responseCalib.cpp
 *
 *  @param  G inverse response function
 *  @param  E scene irradiance
 *  @param  exposureVec vector of exposures
 *  @param  dataVec vector of images
 *  @param  wh width * height
 *  @return 2d point
 */ 
Eigen::Vector2d rmse(double* G, double* E, std::vector<double> &exposureVec, std::vector<unsigned char*> &dataVec,  int wh)
{
	long double e=0;		// yeah - these will be sums of a LOT of values, so we need super high precision.
	long double num=0;

	int n = dataVec.size();
	for(int i=0;i<n;i++)
	{
		for(int k=0;k<wh;k++)
		{
			if(dataVec[i][k] == 255) continue;
			double r = G[dataVec[i][k]] - exposureVec[i]*E[k];
			if(!std::isfinite(r)) continue;
			e += r*r*1e-10;
			num++;
		}
	}

	return Eigen::Vector2d(1e5*sqrtl((e/num)), (double)num);
}

/** 
 *  @brief  Main function
 *
 *  @param  argc number of arguments
 *  @param  argv arguments passed in the command line
 *  @return return status
 */ 
int main(int argc, char** argv)
{
	// Parse arguments
	process_program_options(argc, argv);

	cout << "Starting " << argv[0] << "..." << endl;
	cout << "Settings " << endl
		<< "\t- Source: " << video_stream_provider << endl
		<< "\t- frames to take: " << n_frames << endl
		<< "\t- Visual: " << ((visual) ? "y" : "n") << endl
		<< "\t- Output gamma file: " << gamma_calib << endl;

	//  The source is a number of device, (e.g.: 0 would be /dev/video0)
	if (video_stream_provider.size() < 2)
	{
		cap.open(atoi(video_stream_provider.c_str()));
	}
	else
	{
		cap.open(video_stream_provider);
	}

	// Try to open the video source
	if (!cap.isOpened())
	{
		cerr << "! Could not open the stream." << endl;
		exit(EXIT_FAILURE);
	}

	// Create the window
	if (visual)
		cv::namedWindow("Video", cv::WINDOW_AUTOSIZE);

	vector<double> exposureVec;
	vector<unsigned char*> dataVec;

	cv::Mat frame;			// current frame
	int counter = 0;		// frame counter

	int w, h, size;			// frame width and height and number of frames taken

	// Acquire n_frames frames/images
	while (counter < n_frames)
	{
		// Get a new frame
		cap >> frame;

		if (frame.empty())
		{
			cerr << "! Acquisition stopped before expected (" << counter << " frames acquired out of " << n_frames <<")" << endl; 

			if (counter == 0)
				exit(EXIT_FAILURE);
			else
				break;
		}

		if (visual)
		{
			cv::imshow("Video", frame);
			cv::waitKey(1);
		}

		if (frame.rows == 0 || frame.cols == 0) continue;

		w = frame.cols;
		h = frame.rows;

		unsigned char* data = new unsigned char[w*h];
		memcpy(data, frame.data, frame.rows * frame.cols);

		dataVec.push_back(data);
		exposureVec.push_back(1.0);

		unsigned char* data2 = new unsigned char[ frame.rows * frame.cols];
		for (unsigned int it = 0; it < leakPadding; it++)
		{
			memcpy(data2, data, frame.rows * frame.cols);

			for(int y=1;y<h-1;y++)
			{
				for(int x=1;x<w-1;x++)
				{
					if(data[x+y*w]==255)
					{
						data2[x+1 + w*(y+1)] = 255;
						data2[x+1 + w*(y  )] = 255;
						data2[x+1 + w*(y-1)] = 255;

						data2[x   + w*(y+1)] = 255;
						data2[x   + w*(y  )] = 255;
						data2[x   + w*(y-1)] = 255;

						data2[x-1 + w*(y+1)] = 255;
						data2[x-1 + w*(y  )] = 255;
						data2[x-1 + w*(y-1)] = 255;
					}
				}
			}

			memcpy(data, data2, frame.rows * frame.cols);
		}
		delete[] data2;

		counter++;
	}

	if (visual)
		cv::destroyAllWindows();

	// Close acquisition
	cap.release();

	size = dataVec.size();

	cout << "Correctly acquired " << dataVec.size() << " frames." << endl; 
	cout << "Now I'll do the magic... " << endl; 

	double* E = new double[w*h];		// scene irradiance
	double* En = new double[w*h];		// scene irradiance
	double* G = new double[256];		// inverse response function

	// set starting scene irradiance to mean of all images.
	memset(E, 0, sizeof(double)*w*h);
	memset(En ,0, sizeof(double)*w*h);
	memset(G, 0, sizeof(double)*256);

	for (unsigned int i = 0; i < size; i++)
		for (unsigned int k=0; k<w*h;k++)
		{
			//if(dataVec[i][k]==255) continue;
			E[k] += dataVec[i][k];
			En[k] ++;
		}
	for(int k=0;k<w*h;k++) E[k] = E[k]/En[k];

	bool optE = true;
	bool optG = true;

	for(int it=0;it<nits;it++)
	{
		if(optG)
		{
			// optimize log inverse response function.
			double* GSum = new double[256];
			double* GNum = new double[256];
			memset(GSum,0,256*sizeof(double));
			memset(GNum,0,256*sizeof(double));
			for(int i=0;i<size;i++)
			{
				for(int k=0;k<w*h;k++)
				{
					int b = dataVec[i][k];
					if(b == 255) continue;
					GNum[b]++;
					GSum[b]+= E[k] * exposureVec[i];
				}
			}
			for(int i=0;i<256;i++)
			{
				G[i] = GSum[i] / GNum[i];
				if(!std::isfinite(G[i]) && i > 1) G[i] = G[i-1] + (G[i-1]-G[i-2]);
			}
			delete[] GSum;
			delete[] GNum;
			cout << "optG RMSE = " << rmse(G, E, exposureVec, dataVec, w*h )[0] << endl;
		}


		if(optE)
		{
			// optimize scene irradiance function.
			double* ESum = new double[w*h];
			double* ENum = new double[w*h];
			memset(ESum,0,w*h*sizeof(double));
			memset(ENum,0,w*h*sizeof(double));
			for(int i=0;i<size;i++)
			{
				for(int k=0;k<w*h;k++)
				{
					int b = dataVec[i][k];
					if(b == 255) continue;
					ENum[k] += exposureVec[i]*exposureVec[i];
					ESum[k] += (G[b]) * exposureVec[i];
				}
			}
			for(int i=0;i<w*h;i++)
			{
				E[i] = ESum[i] / ENum[i];
				if(E[i] < 0) E[i] = 0;
			}

			delete[] ENum;
			delete[] ESum;
			cout << "OptE RMSE = " << rmse(G, E, exposureVec, dataVec, w*h )[0] << endl;
		}


		// rescale such that maximum response is 255 (fairly arbitrary choice).
		double rescaleFactor=255.0 / G[255];
		for(int i=0;i<w*h;i++)
		{
			E[i] *= rescaleFactor;
			if(i<256) G[i] *= rescaleFactor;
		}
		Eigen::Vector2d err = rmse(G, E, exposureVec, dataVec, w*h );
		cout << "resc RMSE = " << err[0] << "; rescale with " << rescaleFactor << endl;
	}


	cout << "Writing gamma calibration file..." << endl;
	std::ofstream lg;
	lg.open("pcalib.txt", std::ios::trunc | std::ios::out);
	lg.precision(15);
	for (int i=0;i<256;i++)
		lg << G[i] << " ";
	lg << "\n";
	lg.flush();
	lg.close();

	cout << "Everything went well." << endl;
	cout << "Cleaning memory..." << endl;

	delete[] E;
	delete[] En;
	delete[] G;
	for (int i=0;i<size;i++) delete[] dataVec[i];
	return 0;

	cout << "We are done here." << endl << endl;
	exit(EXIT_SUCCESS);
}